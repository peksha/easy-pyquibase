###################################
#       Easy pyquibase v0.1       #
###################################

This tool is based on Pyquibase
    https://github.com/rampart81/pyquibase
and extended for oracle database. GUI created
with possibility of: 
* generate output SQL file based on given changelog file
  using liquibase updateSQL command
* deploy changes from changelog file using liquibase update
  command

Installation steps for GNU/Linux Debian based OS:
1. Install Python3
    sudo apt-get install python3

2. Install PySimpleGUI, pyyaml and python3-tk packages
    pip3 install PySimpleGUI
    pip3 install pyyaml
    sudo apt-get install python3-tk

4. git clone https://gitlab.com/peksha/easy-pyquibase.git

5. If you would like to connect to Oracle database, download
   JDBC driver from Oracle sites, rename it as "ojdbc6.jar" and
   copy it into easy-pyquibase/pyquibase/db-connectors directory

6. run easy-pyquibase with bash script
    cd easy-pyquibase
    ./easy_pyquibase.sh

Installation steps for Windows OS:
1. Install latest version of Python3 from address
    https://www.python.org/downloads/windows/
   during installation please check pip and Tkinter to 
   install together with python

2. Add python3 into PATH

3. Install PySimpleGUI and pyyaml
    pip3.7.exe install PySimpleGUI
    pip3.7.exe install pyyaml

4. git clone https://gitlab.com/peksha/easy-pyquibase.git

5. If you would like to connect to Oracle database, download
   JDBC driver from Oracle sites, rename it as "ojdbc6.jar" and
   copy it into easy-pyquibase/pyquibase/db-connectors directory

6. run easy-pyquibase with double click on easy_pyquibase.py file
   or from command line 
    python.exe easy_pyquibase.py
