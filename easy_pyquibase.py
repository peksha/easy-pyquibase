import PySimpleGUI as sg
import subprocess
from pyquibase.pyquibase import Pyquibase
import yaml

sg.ChangeLookAndFeel('Black')

layout = [[sg.Button('Read config'), sg.Button('Write config')],
          [sg.Text('DB name', size=(20, 1)), sg.Input('', key='_DBNAME_')],
          [sg.Text('DB host', size=(20, 1)), sg.Input('', key='_DBHOST_')],
          [sg.Text('DB port', size=(20, 1)), sg.Input('1521', key='_DBPORT_')],
          [sg.Text('DB user', size=(20, 1)), sg.Input('appdeploy', key='_USER_')],
		  [sg.Text('DB password', size=(20, 1)), sg.Input('', key='_PWD_')],
          [sg.Text('Changelog XML file', size=(20, 1)), sg.Input('changelog.xml', key='_CHANGELOG_'), sg.FileBrowse()],
          [sg.Text('Output SQL file', size=(20, 1)), sg.Input('lb_output_file.sql', key='_OUTPUT_'), sg.FileBrowse()],
          [sg.Button('Generate output SQL'), sg.Button('Exit')]]

window = sg.Window('Easy Pyquibase v0.1').Layout(layout)

# The Event Loop
while True:
    event, values = window.Read()
    if event in (None, 'Exit'):
        break
    elif event == 'Read config':
        filename = "config.yml"
        file = open(filename, 'r')
        cfg = yaml.load(file)
        window.FindElement('_DBNAME_').Update(cfg['db'])
        window.FindElement('_DBHOST_').Update(cfg['host'])
        window.FindElement('_DBPORT_').Update(cfg['port'])
        window.FindElement('_USER_').Update(cfg['user'])
        window.FindElement('_PWD_').Update(cfg['pwd'])
        window.FindElement('_CHANGELOG_').Update(cfg['changelog'])
        window.FindElement('_OUTPUT_').Update(cfg['output'])
    elif event == 'Write config':
        cfg = {
            'db': values['_DBNAME_'],
            'host': values['_DBHOST_'],
            'port': values['_DBPORT_'],
            'user': values['_USER_'],
            'pwd': values['_PWD_'],
            'changelog': values['_CHANGELOG_'],
            'output': values['_OUTPUT_']
        }
        with open('config.yml', 'w') as outfile:
            yaml.dump(cfg, outfile, default_flow_style=False)
    elif event == 'Generate output SQL':
        pyquibase = Pyquibase.oracle(
            host            = values['_DBHOST_'],
            port            = values['_DBPORT_'],
            db_name         = values['_DBNAME_'],
            username        = values['_USER_'],
            password        = values['_PWD_'],
            change_log_file = values['_CHANGELOG_']
        )
        pyquibase.updateSQL(values['_OUTPUT_'])
